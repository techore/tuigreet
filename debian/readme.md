# Packing

Notes on building debian binary package.

## Build

Ensure you start with a current and clean clone!

Apply patches.

```
dq push -a
```

Build debian package.

```
cargo deb
```

deb package is written to target/debian/.

## Update

1. Update source code
2. Test patches and fix any failures
3. Update Cargo.toml package and package.metadata.deb stanzas? If true, dq add or new.
4. Update debian/changelog
5. build and test
6. finalize

## Reference

https://crates.io/crates/cargo-deb
